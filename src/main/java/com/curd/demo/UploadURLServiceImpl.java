package com.curd.demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.tomcat.util.json.JSONParser;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.DataInput;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.*;

@Service
public class UploadURLServiceImpl {

//    private static final String FILENAME = "com/example/demo/";

    @Autowired
    MyRepository myRepository;

    public void parseSdnFile() throws ParserConfigurationException, IOException, SAXException {

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        // try {
        // dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);

        DocumentBuilder db = dbf.newDocumentBuilder();
        Resource resource = new ClassPathResource("students.xml");

//        Resource res = new ClassPathResource("students.xml");
//        InputStream is = UploadURLServiceImpl.class.getClass().getClassLoader().getResourceAsStream("/Resources/students.xml");


        Document doc = db.parse(resource.getInputStream());


        doc.getDocumentElement().normalize();

        //Element docEl = doc.getDocumentElement();

        NodeList list = doc.getElementsByTagName("staff");


        for (int temp = 0; temp < list.getLength(); temp++) {

            Node node = list.item(temp);


            if (node.getNodeType() == Node.ELEMENT_NODE) {

                Element element = (Element) node;

                // get staff's attribute
                String id = element.getAttribute("id");

                // get text
                String firstname = element.getElementsByTagName("firstname").item(0).getTextContent();
                String lastname = element.getElementsByTagName("lastname").item(0).getTextContent();
                String nickname = element.getElementsByTagName("nickname").item(0).getTextContent();

                NodeList salaryNodeList = element.getElementsByTagName("salary");
                String salary = salaryNodeList.item(0).getTextContent();

                // get salary's attribute
                String currency = salaryNodeList.item(0).getAttributes().getNamedItem("currency").getTextContent();
                Student student = new Student(firstname, lastname, nickname);

                myRepository.save(student);


            }
        }

//  for (SdnEntryDTO sdnEntryDTO : ofacs) {
//   myRepository.saveAll(ofacs);
//  }
////        } catch (ParserConfigurationException | SAXException | IOException e) {
////            e.printStackTrace();
////        }
    }


    public ResponseEntity<?> getAllUser() {
        List<Student> opUser = myRepository.findAll();
        System.out.println(opUser);
        return new ResponseEntity<>(opUser, HttpStatus.OK);

    }

    public ResponseEntity<?> getAllUrl() throws IOException,ParserConfigurationException, SAXException{
        String str = "https://www.w3schools.com/xml/plant_catalog.xml";
        URL url = new URL(str);
        InputStream is = url.openStream();
        int ptr = 0;
        StringBuilder builder = new StringBuilder();
        while ((ptr = is.read()) != -1) {
            builder.append((char) ptr);
        }

//        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        // try {
        // dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);

//        DocumentBuilder db = dbf.newDocumentBuilder();
//
//        RestTemplate restTemplate = new RestTemplate();
//
//        Object forObject = restTemplate.getForObject("http://www-db.deis.unibo.it/courses/TW/DOCS/w3schools/xml/simple.xml", Element);


        String xml = builder.toString();
//        Document parse = db.parse(xml);
        JSONObject jsonObject = XML.toJSONObject(xml);
        System.out.println(jsonObject);
        Map<String, Object> stringObjectMap = toMap(jsonObject);
        return ResponseEntity.ok(stringObjectMap);
    }

    public static Map<String, Object> toMap(JSONObject jsonobj)  throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();
        Iterator<String> keys = jsonobj.keys();
        while(keys.hasNext()) {
            String key = keys.next();
            Object value = jsonobj.get(key);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }   return map;
    }
    public static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for(int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }
            else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }   return list;
    }
}
