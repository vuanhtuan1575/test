package com.curd.demo.controller;


import com.curd.demo.UploadURLServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

@RestController
@RequestMapping("/api")
public class Controller {
    @Autowired
    UploadURLServiceImpl uploadURLService;
    @GetMapping("/all")
    public Object addOrder()  {

        try {
            uploadURLService.parseSdnFile();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
        return true;

    }
    @ExceptionHandler
    @GetMapping("/ctv-all")
    @Operation(summary = "get all user")
    public ResponseEntity<?> getAllCtv(){
        return uploadURLService.getAllUser();
    }
    @ExceptionHandler
    @GetMapping("/getAll")
    @Operation(summary = "get all user from URL")
    public ResponseEntity<?> getAllUrl() throws IOException, ParserConfigurationException, SAXException {
        return uploadURLService.getAllUrl();
    }

}
