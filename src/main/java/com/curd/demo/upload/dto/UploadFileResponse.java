package com.curd.demo.upload.dto;



@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UploadFileResponse {
	private String fileName;
    private String fileDownloadUri;
    private String fileType;
    private long size;
}